@extends('layouts.app')

@section('content')




<div class="container">
@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close"><span class="fas fa-times"></span></a>
        <p>{{ $message }}</p>
    </div>
@else
    @if ($message = Session::get('fail'))
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert" aria-label="close"><span class="fas fa-times"></span></a>
            <p>{{ $message }}</p>
        </div>
    @endif
@endif



    

    @if  (Auth::user()->role_id == 4) 

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            
            <div class="panel panel-default">


                <div class="panel-heading">Data User</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('edit.User') }}">
                        {{ csrf_field() }}

                        

                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Username</label>

                             <div class="col-md-6">
                                <input type="text" class="form-control" name="username" value="{{ Auth::user()->username }}" readonly>

                                @if ($errors->has('username'))
                                   <span class="help-block">
                                      <strong>{{ $errors->first('username') }}</strong>
                                   </span>
                                @endif
                             </div>
                         </div>
                        
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ Auth::user()->name }}" readonly>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ Auth::user()->email }}" readonly>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="address" class="col-md-4 control-label">Address</label>

                            <div class="col-md-6">
                                <input id="address" type="text" class="form-control" name="address" value="{{ Auth::user()->address }}" readonly>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="phone" class="col-md-4 control-label">Phone Number</label>

                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control" name="phone" value="{{ Auth::user()->hp }}" readonly>
                            </div>
                        </div>

                        <div class="form-group">
                        <label for="phone" class="col-md-4 control-label"><a href="editUserPage/{{ Auth::user()->id }}"  class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Edit</a> </label>
                        </div>

                                                
                    </form>
                </div>
                
            </div>
        </div>
    </div>

    @endif

    @if  (Auth::user()->role_id != 4) 


    <br/>
    <h1 class="text-center">Daftar User</h1>
    <br/>
    @if  (Auth::user()->role_id == 1 || Auth::user()->role_id == 3) 
        <a href="{{route('add.page')}}" class="btn btn-xs btn-primary"><i class="fa fa-plus-circle"></i> Tambah User</a>
    @endif
    <a href="{{route('get.excel')}}" class="btn btn-xs btn-primary"><i class="fa fa-download"></i> Export to Excel</a>
    <a href="{{route('get.json')}}" class="btn btn-xs btn-primary"><i class="fa fa-download"></i> Export to JSON</a>
    <br/><br/>
    <table class="table table-striped table-hover" id="users-table" style="background-color: white;">
        <thead>
            <tr>
                <th></th>
                <th><input type="text" size="10" id="input1" style="font-size: 12px; font-weight: 500;" placeholder="Search Name" onkeyup="searchTable(1)"></th>
                <th><input type="text" size="10" id="input2" style="font-size: 12px; font-weight: 500;" placeholder="Search Email" onkeyup="searchTable(2)"></th>
                <th><input type="text" size="12" id="input3" style="font-size: 12px; font-weight: 500;" placeholder="Search Address" onkeyup="searchTable(3)"></th>
                <th><input type="text" size="10" id="input4" style="font-size: 12px; font-weight: 500;" placeholder="Search Phone" onkeyup="searchTable(4)"></th>
                <th><input type="text" size="12" id="input5" style="font-size: 12px; font-weight: 500;" placeholder="Search Jabatan" onkeyup="searchTable(5)"></th>
                <th><input type="text" size="10" id="input6" style="font-size: 12px; font-weight: 500;" placeholder="Search Role" onkeyup="searchTable(6)"></th>
                <th></th>
            </tr>
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Email</th>
                <th>Address</th>
                <th>Phone</th>
                <th>Jabatan</th>
                <th>Role</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>


    @endif

    <!-- hide datatables default search bar -->
    <style type="text/css"> 
        .dataTables_filter{display: none}
    </style>

    <script>
    $(function() {
        var table = $('#users-table').DataTable({
            processing: true,
            serverSide: true,
            //bFilter: false,
            ajax: '{!! route('get.datas') !!}',
            columns: [
                { data: 'no', name: 'no' , searchable: true},
                { data: 'name', name: 'name' , searchable: true},
                { data: 'email', name: 'email' },
                { data: 'address', name: 'address' },
                { data: 'hp', name: 'hp' },
                { data: 'jabatan', name: 'jabatan' },
                { data: 'role', name: 'role_id' },
                { data: 'action', name: 'action', orderable: false, searchable: false }
            ],
            columnDefs: [{
                "defaultContent": "-",
                "searchable": false,
                "orderable": false,
                "targets": 0
              }]
        });
        
    });

    function searchTable(col) {        
        var table = $('#users-table').DataTable();
        var input = '#input' + col;
        var val = $(input).val();
        table.columns(col).search(val).draw();
    }

    function confirmDel(id, name) {
        var txt;
        var r = confirm("Yakin akan menghapus data? \n\nName: " + name);
        if (r == true) {
            txt = "You pressed OK!"; 
            $('#delButton'+id).click();
        } else {
            txt = "You pressed Cancel!";
        }
    }

    </script>
    @stack('scripts')

</div>
@endsection
