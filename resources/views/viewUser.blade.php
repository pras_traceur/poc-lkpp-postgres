@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <a href="{{ route('home') }}">Daftar User</a> <i class="fa fa-chevron-right"></i> Data User
            </br></br>
            <div class="panel panel-default">


                <div class="panel-heading">Data User</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('edit.User') }}">
                        {{ csrf_field() }}

                        

                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Username</label>

                             <div class="col-md-6">
                                <span class="form-control" name="username" >{{ $user->username }}</span>
                             </div>
                         </div>
                        
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <span class="form-control" name="name" >{{ $user->name }}</span>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <span class="form-control" name="email" >{{ $user->email }}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="address" class="col-md-4 control-label">Address</label>

                            <div class="col-md-6">
                                <span class="form-control" name="address" >{{ $user->address }}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="phone" class="col-md-4 control-label">Phone Number</label>

                            <div class="col-md-6">
                                <span class="form-control" name="phone" >{{ $user->hp }}</span>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('npwp') ? ' has-error' : '' }}">
                            <label for="npwp" class="col-md-4 control-label">NPWP</font></label>

                            <div class="col-md-6">
                                <span id='npwp' class="form-control" name="npwp" >{{ $user->npwp }}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="role" class="col-md-4 control-label">Jabatan</font></label>

                            <div class="col-md-6">
                                @foreach($jabatans as $jabatan)
                                    @if ($user->jabatan_id == $jabatan->id)
                                        <span class="form-control" >{{ $jabatan->name }}</span>
                                    @endif      
                                @endforeach
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="role" class="col-md-4 control-label">Role</label>

                            <div class="col-md-6">
                                @foreach($roles as $role)
                                    @if ($user->role_id == $role->id)
                                        <span class="form-control" >{{ $role->name }}</span>
                                    @endif      
                                @endforeach
                            </div>
                        </div>

                        
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
    $(function() {
        //$("#npwp").mask("99.999.999.9-999.999");
        var a = $("#npwp").text();
        var x = [a.slice(0, 2), '.', a.slice(2, 5), '.', a.slice(5, 8), '.', a.slice(8, 9), '-', a.slice(9, 12), '.', a.slice(12)].join('');
        $("#npwp").text(x);        
    });
    </script>
</div>
@endsection
