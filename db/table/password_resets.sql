/*
 Navicat Premium Data Transfer

 Source Server         : Local Postgres
 Source Server Type    : PostgreSQL
 Source Server Version : 100010
 Source Host           : localhost:5432
 Source Catalog        : poc-lkpp
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 100010
 File Encoding         : 65001

 Date: 02/10/2019 03:49:30
*/


-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS "password_resets";
CREATE TABLE "password_resets" (
  "email" varchar(191) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL,
  "token" varchar(191) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL,
  "created_at" timestamp(0) DEFAULT NULL::timestamp without time zone
)
;
ALTER TABLE "password_resets" OWNER TO "postgres";

-- ----------------------------
-- Indexes structure for table password_resets
-- ----------------------------
CREATE INDEX "password_resets_email_index" ON "password_resets" USING btree (
  "email" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
