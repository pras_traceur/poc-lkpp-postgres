/*
 Navicat Premium Data Transfer

 Source Server         : Local Postgres
 Source Server Type    : PostgreSQL
 Source Server Version : 100010
 Source Host           : localhost:5432
 Source Catalog        : poc-lkpp
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 100010
 File Encoding         : 65001

 Date: 02/10/2019 03:50:00
*/


-- ----------------------------
-- Table structure for items
-- ----------------------------
DROP TABLE IF EXISTS "items";
CREATE TABLE "items" (
  "id" int4 NOT NULL DEFAULT nextval('items_seq'::regclass),
  "nama" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "deskripsi" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "harga" float8 DEFAULT NULL,
  "stok" int4 DEFAULT NULL,
  "created_date" timestamp(0) DEFAULT NULL::timestamp without time zone,
  "updated_date" timestamp(0) DEFAULT NULL::timestamp without time zone
)
;
ALTER TABLE "items" OWNER TO "postgres";

-- ----------------------------
-- Records of items
-- ----------------------------
BEGIN;
INSERT INTO "items" VALUES (1, 'Laptop 1', 'Laptop nomor 1', 5000000, 11, NULL, '2019-02-27 09:29:14');
INSERT INTO "items" VALUES (2, 'Komputer', 'Komputer baru', 1000, 10, NULL, NULL);
INSERT INTO "items" VALUES (3, 'Meja', 'meja belajar', 500000, 5, NULL, NULL);
INSERT INTO "items" VALUES (4, 'Lemari', 'lemari dokumen', 1000000, 2, NULL, NULL);
INSERT INTO "items" VALUES (6, 'Mouse', 'mouse kecil', 200000, 20, NULL, NULL);
INSERT INTO "items" VALUES (7, 'Server', 'server development', 10000000, 2, NULL, NULL);
INSERT INTO "items" VALUES (10, 'Modem', 'modem 4G', 200000, 30, NULL, NULL);
INSERT INTO "items" VALUES (11, 'Router', 'router saja', 300000, 10, NULL, NULL);
INSERT INTO "items" VALUES (5, 'Monitor', 'monitor LED', 2500000, 14, NULL, '2019-10-02 01:24:58');
INSERT INTO "items" VALUES (12, 'Handphone', 'hp merek lokal', 10000000, 38, NULL, '2019-10-02 01:36:52');
INSERT INTO "items" VALUES (8, 'Mobil', 'mobil biasa', 100000000, 10, NULL, NULL);
COMMIT;

-- ----------------------------
-- Primary Key structure for table items
-- ----------------------------
ALTER TABLE "items" ADD CONSTRAINT "items_pkey" PRIMARY KEY ("id");
