/*
 Navicat Premium Data Transfer

 Source Server         : Local Postgres
 Source Server Type    : PostgreSQL
 Source Server Version : 100010
 Source Host           : localhost:5432
 Source Catalog        : poc-lkpp
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 100010
 File Encoding         : 65001

 Date: 02/10/2019 03:48:00
*/


-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS "users";
CREATE TABLE "users" (
  "id" int4 NOT NULL DEFAULT nextval('users_seq'::regclass),
  "name" varchar(500) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL,
  "username" varchar(500) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL,
  "email" varchar(191) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL,
  "address" varchar(1000) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying(1000),
  "hp" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying(20),
  "npwp" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying(20),
  "password" varchar(500) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL,
  "remember_token" varchar(500) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying(500),
  "created_at" timestamp(0) DEFAULT NULL::timestamp without time zone,
  "updated_at" timestamp(0) DEFAULT NULL::timestamp without time zone,
  "role_id" int4 NOT NULL DEFAULT NULL,
  "jabatan_id" int4 DEFAULT NULL
)
;
ALTER TABLE "users" OWNER TO "postgres";

-- ----------------------------
-- Records of users
-- ----------------------------
BEGIN;
INSERT INTO "users" VALUES (38, 'User 2', 'user2', 'user2@bgs.com', 'Jakarta', '1111111111111', '11.111.111.1-111.111', '$2y$10$bszUiEsV5YGynFfKQR1i.OL5hT/IPxPFrivAEkQ32DJYxvcQnLkF.', 'bDexpQ76HIZAf2Mxte8Eh4y1d7bMp5APmKzhEsEdbMzuE6Et8mVK24tQZuvz', '2019-10-02 02:14:33', '2019-10-02 02:14:33', 2, 1);
INSERT INTO "users" VALUES (25, 'Manajer 1', 'manajer1', 'manajer1@bgs.com', 'Jakarta', '0888888888888', '11.111.111.1-111.111', '$2y$10$Jzhv4EfPaTjvIj.8Fr6Nxu1H7fVnbjfRqO3/NTh3qysrqtI.DEr5u', 'acSv1Y3qq1FfehSunJkLqtyZxGGA7oEWYun0xux6NxGBtdscYuaPOCUAW1kF', '2018-02-07 03:14:15', '2018-02-07 03:14:15', 1, 1);
INSERT INTO "users" VALUES (24, 'Karyawan 1', 'karyawan1', 'karyawan1@bgs.com', 'Jakarta', '0888888888888', '11.111.111.1-111.111', '$2y$10$3./4CXAZScwFjLvM9VYozOez6SX2vsHWq.Yd5XE0GMR4lPLQ9Qdx2', 'N0qFNUequJ8v6xdRJXr7mr6KZBxDW1RIQQllPIOWSRwmwxei5OpgZCMx6yh4', '2018-02-07 01:55:05', '2018-02-07 01:55:05', 3, 2);
INSERT INTO "users" VALUES (16, 'Administrator', 'admin', 'admin1@bgs.com', 'Jakarta', '0888888888888', '11.111.111.1-111.111', '$2y$10$7BFDVTq.3wiEN7KmoZ3z4.FxjR6b2l4Of006pY3AHwSB8K5rxOadu', '9y2zJ3rMHIRN6Wq7PmUhNHRoAwH4h8zLhAUR5RXtKsm3tiG3lIln1w8fougS', '2018-02-05 04:45:14', '2018-02-05 04:45:14', 1, 1);
INSERT INTO "users" VALUES (29, 'User 1', 'user1', 'user1@bgs.com', 'Jakarta', '0888888888888', '11.111.111.1-111.111', '$2y$10$OFWRT8EqmvKVVxn8F9RQQeV8etyc/E0kmIbM3h07hcTkW9KlMuo4K', 'VtlDauMKP4wNCdcNHfgc9TlHejX44sjUhTv3AZJ5Qkl8jKPDwn04P9STXTkb', NULL, NULL, 2, 6);
COMMIT;

-- ----------------------------
-- Indexes structure for table users
-- ----------------------------
CREATE INDEX "users_role_id_foreign" ON "users" USING btree (
  "role_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);

-- ----------------------------
-- Uniques structure for table users
-- ----------------------------
ALTER TABLE "users" ADD CONSTRAINT "users_email_unique" UNIQUE ("email");
ALTER TABLE "users" ADD CONSTRAINT "users_username_unique" UNIQUE ("username");

-- ----------------------------
-- Checks structure for table users
-- ----------------------------
ALTER TABLE "users" ADD CONSTRAINT "users_role_id_check" CHECK ((role_id > 0));
ALTER TABLE "users" ADD CONSTRAINT "users_id_check" CHECK ((id > 0));

-- ----------------------------
-- Primary Key structure for table users
-- ----------------------------
ALTER TABLE "users" ADD CONSTRAINT "users_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Foreign Keys structure for table users
-- ----------------------------
ALTER TABLE "users" ADD CONSTRAINT "users_role_id_foreign" FOREIGN KEY ("role_id") REFERENCES "roles" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
