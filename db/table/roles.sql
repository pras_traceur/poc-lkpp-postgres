/*
 Navicat Premium Data Transfer

 Source Server         : Local Postgres
 Source Server Type    : PostgreSQL
 Source Server Version : 100010
 Source Host           : localhost:5432
 Source Catalog        : poc-lkpp
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 100010
 File Encoding         : 65001

 Date: 02/10/2019 03:50:19
*/


-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS "roles";
CREATE TABLE "roles" (
  "id" int4 NOT NULL DEFAULT nextval('roles_seq'::regclass),
  "name" varchar(191) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL,
  "created_at" timestamp(0) DEFAULT NULL::timestamp without time zone,
  "updated_at" timestamp(0) DEFAULT NULL::timestamp without time zone
)
;
ALTER TABLE "roles" OWNER TO "postgres";

-- ----------------------------
-- Records of roles
-- ----------------------------
BEGIN;
INSERT INTO "roles" VALUES (1, 'ADMIN', '2018-01-30 11:31:19', '2018-01-30 11:31:22');
INSERT INTO "roles" VALUES (2, 'USER', '2018-01-30 11:31:36', '2018-01-30 11:31:39');
INSERT INTO "roles" VALUES (3, 'CREATE', NULL, NULL);
INSERT INTO "roles" VALUES (4, 'DETAIL', NULL, NULL);
COMMIT;

-- ----------------------------
-- Checks structure for table roles
-- ----------------------------
ALTER TABLE "roles" ADD CONSTRAINT "roles_id_check" CHECK ((id > 0));

-- ----------------------------
-- Primary Key structure for table roles
-- ----------------------------
ALTER TABLE "roles" ADD CONSTRAINT "roles_pkey" PRIMARY KEY ("id");
