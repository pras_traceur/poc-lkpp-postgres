/*
 Navicat Premium Data Transfer

 Source Server         : Local Postgres
 Source Server Type    : PostgreSQL
 Source Server Version : 100010
 Source Host           : localhost:5432
 Source Catalog        : poc-lkpp
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 100010
 File Encoding         : 65001

 Date: 02/10/2019 03:48:12
*/


-- ----------------------------
-- Table structure for transaksis
-- ----------------------------
DROP TABLE IF EXISTS "transaksis";
CREATE TABLE "transaksis" (
  "id" int4 NOT NULL DEFAULT nextval('transaksis_seq'::regclass),
  "jenis" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "id_user" int4 DEFAULT NULL,
  "id_items" int4 DEFAULT NULL,
  "jumlah_items" int4 DEFAULT NULL,
  "created_date" timestamp(0) DEFAULT NULL::timestamp without time zone,
  "nama_user" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "nama_item" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "harga" float8 DEFAULT NULL
)
;
ALTER TABLE "transaksis" OWNER TO "postgres";

-- ----------------------------
-- Records of transaksis
-- ----------------------------
BEGIN;
INSERT INTO "transaksis" VALUES (11, 'Pembelian', 29, 12, 1, '2019-10-02 01:36:52', 'User 1', 'Handphone', 10000000);
COMMIT;

-- ----------------------------
-- Primary Key structure for table transaksis
-- ----------------------------
ALTER TABLE "transaksis" ADD CONSTRAINT "transaksis_pkey" PRIMARY KEY ("id");
