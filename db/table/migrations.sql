/*
 Navicat Premium Data Transfer

 Source Server         : Local Postgres
 Source Server Type    : PostgreSQL
 Source Server Version : 100010
 Source Host           : localhost:5432
 Source Catalog        : poc-lkpp
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 100010
 File Encoding         : 65001

 Date: 02/10/2019 03:49:42
*/


-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS "migrations";
CREATE TABLE "migrations" (
  "id" int4 NOT NULL DEFAULT nextval('migrations_seq'::regclass),
  "migration" varchar(191) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL,
  "batch" int4 NOT NULL DEFAULT NULL
)
;
ALTER TABLE "migrations" OWNER TO "postgres";

-- ----------------------------
-- Records of migrations
-- ----------------------------
BEGIN;
INSERT INTO "migrations" VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO "migrations" VALUES (3, '2018_01_30_033034_create_roles_table', 1);
INSERT INTO "migrations" VALUES (4, '2014_10_12_000000_create_users_table', 2);
INSERT INTO "migrations" VALUES (5, '2018_02_08_074032_create_jabatans_table', 3);
COMMIT;

-- ----------------------------
-- Checks structure for table migrations
-- ----------------------------
ALTER TABLE "migrations" ADD CONSTRAINT "migrations_id_check" CHECK ((id > 0));

-- ----------------------------
-- Primary Key structure for table migrations
-- ----------------------------
ALTER TABLE "migrations" ADD CONSTRAINT "migrations_pkey" PRIMARY KEY ("id");
