/*
 Navicat Premium Data Transfer

 Source Server         : Local Postgres
 Source Server Type    : PostgreSQL
 Source Server Version : 100010
 Source Host           : localhost:5432
 Source Catalog        : poc-lkpp
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 100010
 File Encoding         : 65001

 Date: 02/10/2019 03:49:52
*/


-- ----------------------------
-- Table structure for jabatans
-- ----------------------------
DROP TABLE IF EXISTS "jabatans";
CREATE TABLE "jabatans" (
  "id" int4 NOT NULL DEFAULT nextval('jabatans_seq'::regclass),
  "name" varchar(100) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL,
  "created_at" timestamp(0) DEFAULT NULL::timestamp without time zone,
  "updated_at" timestamp(0) DEFAULT NULL::timestamp without time zone
)
;
ALTER TABLE "jabatans" OWNER TO "postgres";

-- ----------------------------
-- Records of jabatans
-- ----------------------------
BEGIN;
INSERT INTO "jabatans" VALUES (1, 'Manajer', NULL, NULL);
INSERT INTO "jabatans" VALUES (2, 'Karyawan', NULL, NULL);
INSERT INTO "jabatans" VALUES (6, 'User', NULL, NULL);
COMMIT;

-- ----------------------------
-- Checks structure for table jabatans
-- ----------------------------
ALTER TABLE "jabatans" ADD CONSTRAINT "jabatans_id_check" CHECK ((id > 0));

-- ----------------------------
-- Primary Key structure for table jabatans
-- ----------------------------
ALTER TABLE "jabatans" ADD CONSTRAINT "jabatans_pkey" PRIMARY KEY ("id");
