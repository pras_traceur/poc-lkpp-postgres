<?php

namespace App\Api\V1\Controllers;

use Illuminate\Http\Request;
use Tymon\JWTAuth\JWTAuth;
use App\Users;
use Dingo\Api\Routing\Helpers;
use App\Http\Controllers\Controller;
use \Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UsersController extends Controller {

  use Helpers;

  public function index(JWTAuth $JWTAuth) {

    $currentUser = $JWTAuth->parseToken()->authenticate();

    return $currentUser
                    ->users()
                    ->orderBy('name', 'asc')
                    ->get()
                    ->toArray();
  }

  public function store(Request $request, JWTAuth $JWTAuth) {

    $currenUser = $JWTAuth->parseToken()->authenticate();

    $users = new Users;

    $users->name = $request->get('name');
    $users->username = $request->get('username');
    $users->email = $request->get('email');
    $users->address = $request->get('address');
    $users->hp = $request->get('hp');
    $users->npwp = $request->get('npwp');
    $users->role_id = $request->get('role_id');
    $users->jabatan_id = $request->get('jabatan_id');

    if ($currenUser->users()->save($users)) {
      return $this->response->created();
    }

    return $this->response->error('could_not_create_book', 500);
  }

  public function show($id, JWTAuth $JWTAuth) {

    $currentUser = $JWTAuth->parseToken()->authenticate();

    $users = $currentUser->users()->find($id);

    if (!$users) {
      throw new NotFoundHttpException;
    }

    return $users;
  }

  public function update(Request $request, $id, JWTAuth $JWTAuth) {

    $currentUser = $JWTAuth->parseToken()->authenticate();

    $users = $currentUser->users()->find($id);

    if (!$users) {
      throw new NotFoundHttpException;
    }

    $users->fill($request->all());

    if ($users->save()) {
      return $this->response->noContent();
    }

    return $this->response->error('could_not_update_book', 500);
  }

  public function destroy($id, JWTAuth $JWTAuth) {

    $currentUser = $JWTAuth->parseToken()->authenticate();

    $users = $currentUser->users()->find($id);

    if (!$users) {
      throw new NotFoundHttpException;
    }

    if ($users->delete()) {
      return $this->response->noContent();
    }

    return $this->response->error('could_not_delete_book', 500);
  }

}
