<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// User -----------------------------------------------------------------------------------

Route::get('addUserPage', function () {return view('addUser');});
Route::get('addPage',['as'=>'add.page','uses'=>'UsersController@addUserPage']);
Route::get('editUserPage/{id}',['as'=>'edit.page','uses'=>'UsersController@editUserPage']);
Route::get('viewUserPage/{id}',['as'=>'view.page','uses'=>'UsersController@viewUserPage']);
Route::get('delUser/{id}', ['as'=>'delete.User','uses'=>'UsersController@deleteUser']);
Route::get('get-users', ['as'=>'get.data','uses'=>'UsersController@getData']);
Route::get('get-users1', ['as'=>'get.datas','uses'=>'UsersController@getDatas']);
Route::get('get-excel', ['as'=>'get.excel','uses'=>'UsersController@getBladeExcel']);
Route::get('get-json', ['as'=>'get.json','uses'=>'UsersController@getJson']);
Route::post('addUser', ['as'=>'add.User','uses'=>'UsersController@addUser']);
Route::post('editUser', ['as'=>'edit.User','uses'=>'UsersController@editUser']);
Route::get('profile', ['as'=>'profile','uses'=>'UsersController@profile']);

// Role -----------------------------------------------------------------------------------

Route::get('listRole', function () {return view('role.listRole');})->name('listRole');
Route::get('addRolePage', function () {return view('role.addRole');})->name('addRole.page');
Route::get('editRolePage/{id}',['as'=>'editRole.page','uses'=>'RolesController@editRolePage']);
Route::get('delRole/{id}', ['as'=>'deleteRole.User','uses'=>'RolesController@deleteRole']);
Route::get('get-roles', ['as'=>'getRoles.datas','uses'=>'RolesController@getDatas']);
Route::post('addRole', ['as'=>'add.Role','uses'=>'RolesController@addRole']);
Route::post('editRole', ['as'=>'edit.Role','uses'=>'RolesController@editRole']);

// Jabatan -----------------------------------------------------------------------------------

Route::get('listJabatan', function () {return view('jabatan.listJabatan');})->name('listJabatan');
Route::get('addJabatanPage', function () {return view('jabatan.addJabatan');})->name('addJabatan.page');
Route::get('editJabatanPage/{id}',['as'=>'editJabatan.page','uses'=>'JabatansController@editJabatanPage']);
Route::get('delJabatan/{id}', ['as'=>'deleteJabatan.User','uses'=>'JabatansController@deleteJabatan']);
Route::get('get-jabatans', ['as'=>'getJabatans.datas','uses'=>'JabatansController@getDatas']);
Route::post('addJabatan', ['as'=>'add.Jabatan','uses'=>'JabatansController@addJabatan']);
Route::post('editJabatan', ['as'=>'edit.Jabatan','uses'=>'JabatansController@editJabatan']);

// Item -----------------------------------------------------------------------------------

Route::get('listItem', function () {return view('item.listItem');})->name('listItem');
Route::get('addItemPage', function () {return view('item.addItem');})->name('addItem.page');
Route::get('editItemPage/{id}',['as'=>'editItem.page','uses'=>'ItemsController@editItemPage']);
Route::get('delItem/{id}', ['as'=>'deleteItem.User','uses'=>'ItemsController@deleteItem']);
Route::get('get-items', ['as'=>'getItems.datas','uses'=>'ItemsController@getDatas']);
Route::post('addItem', ['as'=>'add.Item','uses'=>'ItemsController@addItem']);
Route::post('editItem', ['as'=>'edit.Item','uses'=>'ItemsController@editItem']);
Route::get('get-excelItem', ['as'=>'get.excelItem','uses'=>'ItemsController@getBladeExcel']);
Route::get('get-jsonItem', ['as'=>'get.jsonItem','uses'=>'ItemsController@getJson']);

Route::get('listShop', function () {return view('item.listShop');})->name('listShop');
Route::post('beliItem', ['as'=>'beli.Item','uses'=>'ItemsController@beliItem']);

// Transaksi -----------------------------------------------------------------------------------

Route::get('listTransaksi', function () {return view('transaksi.listTransaksi');})->name('listTransaksi');
Route::get('get-transaksis', ['as'=>'getTransaksi.datas','uses'=>'TransaksisController@getDatas']);
Route::get('get-excelTransaksi', ['as'=>'get.excelTransaksi','uses'=>'TransaksisController@getBladeExcel']);

// Web Server -----------------------------------------------------------------------------------
Route::get('getAllUser', ['as'=>'getAllUser','uses'=>'ApiController@getAllUser']);

Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password1.request');
Route::post('password/reset', 'Auth\ResetPasswordController@postReset')->name('password1.reset');




